import os
from config import TRAIN_PATH, DEV_PATH,TEST_PATH
from config import WORD_VEC_100, LR, INPUT_DROP_SINGLE,DECAY_STEPS,LSTM_DROP_SINGLE, BI_DIRECTION, BI_GRAM,WORD_DIM, LSTM_DIM, TASK_NAME, STACK_STATUS, LSTM_NET, WORD_SINGLE, TRAIN_PATH

from nltk.util import ngrams
import random
import csv
from collections import Counter
from voc import Vocab, OOV, Tag
from prepare_data_index import Data_index


def split_dataset(path_list, dev_ratio=0.001, test_ratio=0.001):
    with open(TRAIN_PATH, 'w',encoding='utf-8') as train:
        sentences = []
        for path in path_list:
             with open(path, 'r',encoding='utf-8') as f:
                 sentences = sentences + f.readlines()

        size = len(sentences)

        dev_size = int(size * dev_ratio)
        test_size = int(size * test_ratio)

        random.shuffle(sentences)

        with open(DEV_PATH, 'w',encoding='utf-8') as dev:
            dev.writelines(sentences[:dev_size])
        with open(TEST_PATH, 'w',encoding='utf-8') as dev:
            dev.writelines(sentences[dev_size:dev_size + test_size])
        with open(TRAIN_PATH, 'w',encoding='utf-8') as train:
            train.writelines(sentences[dev_size + test_size:])
    return sentences[:dev_size],sentences[dev_size:dev_size + test_size],sentences[dev_size + test_size:]

def get_normal_word_table(path):
    f_train = open('./models/table.csv', 'w')
    output_train = csv.writer(f_train)
    
    table_set = []
    with open(path, 'r',encoding='utf-8') as f:
            line = f.readline().strip().split(" ")
            N, dim = map(int, line)
            for k in range(N):
                sentence = f.readline()
                line = sentence.strip().split(" ")
#                print(len(line))
#                print(WORD_DIM)
                table_set.append(line[0])
                output_train.writerow(line[0])
            f_train.close()
            return set(table_set)
def ishan(text):
    # for python 3.x
    # sample: ishan('一') == True, ishan('我&&你') == False
    return all('\u4e00' <= char <= '\u9fff' for char in text)

def process_data(path):
    data = []
    label = []
    out_of_table_num = []
    lengths = []
    start_with_no_chinese = []
    end_with_no_chinese = []
    f = open(path, 'r', encoding='utf-8')
    li = f.readlines()
    f.close()

    # process each sentence.
    for line in li:
        # a list contains words
        sentence = line.strip().replace('\n', '').replace('\r', '').replace('\u3000',' ').split(' ')
        sentence = [w for w in sentence if w != '']
        if len(sentence) == 1 and sentence[0]=='':
            continue
        if sentence == None or len(sentence)==0:
            continue
        data_sentence = []
        label_sentence = []
        for word in sentence:
            for ch in word:
                data_sentence.append(ch)
            if(len(word)==1):
                label_sentence.append('S')
            elif(len(word)==2):
                label_sentence.append('B')
                label_sentence.append('E')
            else:
                k = len(word) - 2
                label_sentence.append('B')
                for i in range(k):
                    label_sentence.append('I')
                label_sentence.append('E')
        label.append(label_sentence)
        data.append(data_sentence)
    print('processing statistics')
    
    for sentence in data:
        num = 0
        length = 0
        start_with_no = 0
        end_with_no = 0
        for i in range(len(sentence)):
            word = sentence[i]
            length += 1
            if not ishan(word):
                if i == 0:
                    start_with_no = 1
                if i == len(sentence) - 1:
                    end_with_no = 1
                num += 1
        start_with_no_chinese.append(start_with_no)
        end_with_no_chinese.append(end_with_no)
        lengths.append(length)
        out_of_table_num.append(num)
    return data,label,out_of_table_num,lengths,start_with_no_chinese,end_with_no_chinese
        

def balance_sample():
    path_msr = './icwb2-data/training/msr_training.utf8'
    path_as ='./icwb2-data/training/as_training'
    path_city = './icwb2-data/training/cityu_training'
    path_pku='./icwb2-data/training/pku_training.utf8'
    
    msr = open(path_msr, 'r',encoding='utf-8')
    as_ = open(path_as, 'r',encoding='utf-8')
    city = open(path_city, 'r',encoding='utf-8')
    pku = open(path_pku, 'r',encoding='utf-8')

    
    
    msr_set = msr.readlines()
    as_set = as_.readlines()
    city_set = city.readlines()
    pku_set = pku.readlines() * 2
    
    random.shuffle(msr_set)
    random.shuffle(as_set)
    random.shuffle(city_set)
    random.shuffle(pku_set)
    
    print(type(pku_set))
    dev = as_set[:250] + city_set[:250] + pku_set[:250] + msr_set[:250]
    train = as_set[250:int(len(as_set)/2)] + city_set[250:] + pku_set[250:] + msr_set[250:]
    test = as_set[-10:]
    print(test)
    
    random.shuffle(dev)
    random.shuffle(train)
    random.shuffle(test)
    
    with open(DEV_PATH, 'w',encoding='utf-8') as d:
        d.writelines(dev)
    with open(TEST_PATH, 'w',encoding='utf-8') as t:
        t.writelines(test)
    with open(TRAIN_PATH, 'w',encoding='utf-8') as t:
        t.writelines(train)
#    sentences = []
#    for path in path_list:
#        with  as f:
#            sentences = sentences + f.readlines()

if __name__ == '__main__':
    balance_sample()
#    table_path = './models/vec100.txt'
#    # merge the dataset
#    path_msr = './icwb2-data/training/msr_training.utf8'
#    path_as ='./icwb2-data/training/as_training'
#    path_city = './icwb2-data/training/cityu_training'
#    path_pku='./icwb2-data/training/pku_training.utf8'
#    path_list = []
#    path_list.append(path_msr)
###   
#    path_list.append(path_pku)
#    path_list.append(path_pku)
#    path_list.append(path_city)
##    
#    path_list.append(path_as)
#    
#    table = get_normal_word_table(table_path)
#    
#    
#    for path in path_list:
#        data,label,out_of_table_num,lengths,start_with_no_chinese,end_with_no_chinese = process_data(path)
#        print(path)
#        print(sum(out_of_table_num)/float(sum(lengths)))
#        print(sum(lengths)/len(lengths))
#        print(sum(start_with_no_chinese)/len(start_with_no_chinese))
#        print(sum(end_with_no_chinese)/len(end_with_no_chinese))
#        print('==================================================')
#    
#    # ==================================================
    print('Generate words and characters need to be trained')
    VOCABS = Vocab(WORD_VEC_100, WORD_SINGLE, single_task=False, bi_gram=BI_GRAM, frequency=5)
    TAGS = Tag()
    init_embedding = VOCABS.word_vectors
    da_idx = Data_index(VOCABS, TAGS)
    da_idx.process_all_data(BI_GRAM, multitask=False)


#c = generate_words_for_training(path,'')random.shuffle(sentences)random.shuffle(msr_set)