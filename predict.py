#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 13:07:01 2019

@author: Kai Niu
"""

import os
import datetime
import numpy as np
import pandas as pd
import tensorflow as tf
import sys
import csv
from sklearn.metrics import accuracy_score, confusion_matrix

from voc import Vocab, OOV, Tag
from config import WORD_VEC_100, LR, INPUT_DROP_SINGLE,DECAY_STEPS,LSTM_DROP_SINGLE, BI_DIRECTION, BI_GRAM,WORD_DIM, LSTM_DIM, TASK_NAME, STACK_STATUS, LSTM_NET, WORD_SINGLE, TRAIN_PATH
from config import TRAIN_DATA_MT, TRAIN_PATH, DEV_DATA_MT, DEV_PATH, TEST_DATA_MT, TEST_PATH, WORD_DICT
from config import TRAIN_DATA_BI, DEV_DATA_BI, TEST_DATA_BI, TRAIN_DATA_UNI, DEV_DATA_UNI, TEST_DATA_UNI
from Baseline_model import Model
from Baseline_train import FLAGS,final_test_step,evaluate_word_PRF,VOCABS,TAGS
import data_helpers
import logging
from prepare_data_index import Data_index
from argparse import ArgumentParser
# ==================================================
def predict(input_path, output_path, resources_path):
    """
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the BIES format.

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.

    :param input_path: the path of the input file to predict.
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    init_embedding = VOCABS.word_vectors
    da_idx = Data_index(VOCABS, TAGS)


    if BI_GRAM is False:
        processed_input_path = TEST_DATA_UNI
    else:
        processed_input_path = TEST_DATA_BI

    f_test = open(processed_input_path, 'w')

    output_test = csv.writer(f_test)
    output_test.writerow(['words', 'tags', 'length' , 'feature_length', 'start_with_no_chinese' , 'end_with_no_chinese' , 'out_of_table_num'])


    length_df = da_idx.process_file(input_path, output_test , BI_GRAM)
    
    
    print(processed_input_path)
    test_df = pd.read_csv(processed_input_path)
    print('test_df length ', test_df.shape)

    test_data_iterator = data_helpers.BucketedDataIterator(test_df, test = True, num_buckets=1)

    with tf.Graph().as_default():
        session_conf = tf.ConfigProto(
          allow_soft_placement=FLAGS.allow_soft_placement,
          log_device_placement=FLAGS.log_device_placement)

        session_conf.gpu_options.allow_growth = True
        sess = tf.Session(config=session_conf)
        with sess.as_default():

            model = Model(batch_size=FLAGS.batch_size,
                  vocab_size=FLAGS.vocab_size,
                  word_dim=FLAGS.word_dim,
                  lstm_dim=FLAGS.lstm_dim,
                  num_classes=FLAGS.num_classes,
                  lr=FLAGS.lr,
                  clip=FLAGS.clip,
                  l2_reg_lambda=FLAGS.l2_reg_lambda,
                  init_embedding=init_embedding,
                  bi_gram=BI_GRAM,
                  stack=STACK_STATUS,
                  lstm_net=LSTM_NET,
                  bi_direction=BI_DIRECTION,
                  )

            # Output directory for models
            try:
                shutil.rmtree(os.path.join(os.path.curdir, "models", FLAGS.model_name))
            except:
                pass
            out_dir = os.path.abspath(os.path.join(os.path.curdir, "models", FLAGS.model_name))
            print("Writing to {}\n".format(out_dir))

            # Checkpoint directory. Tensorflow assumes this directory already exists so we need to create it
            checkpoint_dir = os.path.abspath(os.path.join(out_dir, "checkpoints"))
            filename = 'Base_line_' + str(FLAGS.lr) + '_' + str(FLAGS.input_dropout_keep_prob) \
            + '_' + str(BI_GRAM) + '_' + str(STACK_STATUS) + '_' + str(LSTM_NET) + '_' + str(BI_DIRECTION)
            checkpoint_prefix = os.path.join(checkpoint_dir, filename)
            if not os.path.exists(checkpoint_dir):
                os.makedirs(checkpoint_dir)

            saver = tf.train.Saver(tf.global_variables())

            # Initialize all variables
            sess.run(tf.global_variables_initializer())

            # Load a previous checkpoint if we find one
            latest_checkpoint = tf.train.latest_checkpoint(resources_path)
            if latest_checkpoint:
                print("Loading model checkpoint {}...\n".format(latest_checkpoint))
                saver.restore(sess, latest_checkpoint)


            output_lists = []

            yp, yt = final_test_step(test_df, test_data_iterator, sess, model, test=True, bigram=BI_GRAM)
            print('Begin to output the prediction result''''''''')
            total_length = 0
            for n in length_df:
                output_lists.append( yp[total_length:total_length+n])
                total_length = total_length + n
            print('--------------------------------------------------------------------------')
            evaluate_word_PRF(yp, yt)
            
            labels = ['B','I','E','S']
            with open(output_path,'w') as f:
#                output_label = csv.writer(f)
#                output_label.writerow(['lables'])

                for predict_lable in output_lists:
                    map_label = [labels[i] for i in predict_lable]
                    tmp = ''
                    for ch in map_label:
                        tmp += str(ch) + ','
                    f.write(tmp[:-1] + '\n')                   


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("input_path", help="The path of the input file")
    parser.add_argument("output_path", help="The path of the output file")
    parser.add_argument("resources_path", help="The path of the resources needed to load your model")

    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    predict(args.input_path, args.output_path, args.resources_path)
