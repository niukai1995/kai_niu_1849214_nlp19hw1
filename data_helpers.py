import pandas as pd
import numpy as np
from config import CONCATENATE_NUM,WORD_VEC_100, LR, INPUT_DROP_SINGLE,DECAY_STEPS,LSTM_DROP_SINGLE, BI_DIRECTION, BI_GRAM, TASK_NAME, STACK_STATUS, LSTM_NET, WORD_SINGLE, TRAIN_PATH
'''
This is a until class to provide bath of training data to the Model.
It includes several operations:
    1. sorting the input according to the length of the sentences
    2. split the input into K buckets
    3. shuffle the data in the same bucket
'''
class BucketedDataIterator():
    def __init__(self, df, test = False,num_buckets=10):
        self.df = df
        self.total = len(df)

        # get a DataFram which is sorted by the length column

        # we sort the data in order to padding less zero to the input data, speed up the training procedure
        if not test:
            self.df_sort = df.sort_values('length').reset_index(drop=True)
        else:
            self.df_sort = self.df
        self.size = int(self.total / num_buckets)

        # get a list, each element is a pandas.core.frame.DataFrame object
        self.dfs = []
        for bucket in range(num_buckets - 1):
            self.dfs.append(self.df_sort.iloc[bucket*self.size: (bucket + 1)*self.size])
        self.dfs.append(self.df_sort.iloc[(num_buckets-1)*self.size:])

        self.num_buckets = num_buckets

        # cursor[i] will be the cursor(光标，游标) for the ith bucket
        self.cursor = np.array([0] * num_buckets)
        self.pos = 0
        if not test:
            self.shuffle()

        self.epochs = 0

    def shuffle(self):
        #sorts dataframe by sequence length, but keeps it random within the same length
        for i in range(self.num_buckets):
            self.dfs[i] = self.dfs[i].sample(frac=1).reset_index(drop=True)
            self.cursor[i] = 0

    def next_batch(self, batch_size, bigram=True, round = -1, classifier = False):
        if np.any(self.cursor + batch_size + 1 > self.size):
            self.epochs += 1
            #self.cursor = np.array([0] * self.num_buckets)
            self.shuffle()


        # randomly choose a batch
        ai = np.random.randint(0, self.num_buckets)
        res = self.dfs[ai].iloc[self.cursor[ai]:self.cursor[ai] + batch_size ]

        # get a list whose element is a sentence, a list of words
        words = list(map(lambda x: list(map(int, x.split(","))), res['words'].tolist()))
        tags = list(map(lambda x: list(map(int, x.split(","))), res['tags'].tolist()))


        extra_features = res[['feature_length', 'start_with_no_chinese' , 'end_with_no_chinese' , 'out_of_table_num']]
        self.cursor[ai] += batch_size

        # Pad sequences with 0s so they are all the same length
        maxlen = max(res['length'])



        index = []
        batch_size = extra_features.shape[0]
        sentence_length = maxlen

        for i in range(batch_size):
            for j in range(sentence_length):
                index.append(i+ j *batch_size)
        tmp = pd.concat([extra_features]* sentence_length, ignore_index=True)
        extra_features = tmp.reindex(index)


        if bigram:
            x = np.zeros([batch_size, maxlen * CONCATENATE_NUM], dtype=np.int32)
            for i, x_i in enumerate(x):
                x_i[:res['length'].values[i] * CONCATENATE_NUM] = words[i]
        else:
            x = np.zeros([batch_size, maxlen], dtype=np.int32)
            for i, x_i in enumerate(x):
                x_i[:res['length'].values[i]] = words[i]
        y = np.zeros([batch_size, maxlen], dtype=np.int32)
        for i, y_i in enumerate(y):
            y_i[:res['length'].values[i]] = tags[i]
        if classifier is False:
            return x, y, res['length'].values, extra_features
        else:
            y_class = [round] * batch_size
            return x, y, y_class, res['length'].values



    def next_pred_one(self):
        res = self.df.iloc[self.pos]
        words = list(map(int, res['words'].split(',')))
        tags = list(map(int, res['tags'].split(',')))
        length = res['length']
        self.pos += 1
        if self.pos == self.total:
            self.pos = 0

        extra_features = res[['feature_length', 'start_with_no_chinese' , 'end_with_no_chinese' , 'out_of_table_num']]
        extra_features = pd.DataFrame(np.array(extra_features.values.reshape(-1,4)))
        index = []
        batch_size = 1
        sentence_length = length
#        print('sentence_length ',sentence_length)

        for i in range(batch_size):
            for j in range(sentence_length):
                index.append(i+ j *batch_size)
#        print(index)
        tmp = pd.concat([extra_features]* sentence_length, ignore_index=True)
#        print(tmp)
        extra_features = tmp.reindex(index)

#        print('single extra_features ',s extra_features)
        return np.asarray([words],dtype=np.int32), np.asarray([tags],dtype=np.int32), np.asarray([length],dtype=np.int32),extra_features

    def next_all_batch(self, batch_size, bigram=True):
#        res = self.df.iloc[self.pos : self.pos + batch_size -1]
        res = self.df.iloc[self.pos : self.pos + batch_size]
        words = list(map(lambda x: list(map(int, x.split(","))), res['words'].tolist()))
        tags = list(map(lambda x: list(map(int, x.split(","))), res['tags'].tolist()))

        extra_features = res[['feature_length', 'start_with_no_chinese' , 'end_with_no_chinese' , 'out_of_table_num']]

        self.pos += batch_size
        maxlen = max(res['length'])

        index = []
        batch_size = extra_features.shape[0]
        sentence_length = maxlen

        for i in range(batch_size):
            for j in range(sentence_length):
                index.append(i+ j *batch_size)
        tmp = pd.concat([extra_features]* sentence_length, ignore_index=True)
        extra_features = tmp.reindex(index)

        if bigram:
            x = np.zeros([batch_size, maxlen * CONCATENATE_NUM], dtype=np.int32)
            for i, x_i in enumerate(x):
                x_i[:res['length'].values[i] * CONCATENATE_NUM] = words[i]
        else:
            x = np.zeros([batch_size, maxlen], dtype=np.int32)
            for i, x_i in enumerate(x):
                x_i[:res['length'].values[i]] = words[i]
        y = np.zeros([batch_size, maxlen], dtype=np.int32)
        for i, y_i in enumerate(y):
            y_i[:res['length'].values[i]] = tags[i]
        return x, y, res['length'].values, extra_features

    def print_info(self):
        print( 'dfs shape: ', [len(self.dfs[i]) for i in range(len(self.dfs))])
        print( 'size: ', self.size)

if __name__ == '__main__':
    path = 'data_sxu/dev.csv'
    train_df = pd.read_csv(path)
    words = list(map(lambda x: list(map(int, x.split(","))), train_df['words'].tolist()))
    tags = list(map(lambda x: list(map(int, x.split(","))), train_df['tags'].tolist()))
    train_data_iterator = BucketedDataIterator(train_df,5)
    x, y, lengths,extra_features = train_data_iterator.next_pred_one()
#    x, y, lengths,extra_features = train_data_iterator.next_all_batch(3)
#    x, y, lengths,extra_features = train_data_iterator.next_batch(3)

#BucketedDataIterator()
