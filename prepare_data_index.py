# -*- coding: utf-8 -*-
import csv

from config import  WORD_VEC_100,MAX_LEN,CONCATENATE_NUM
from config import TRAIN_DATA_MT, TRAIN_PATH, DEV_DATA_MT, DEV_PATH, TEST_DATA_MT, TEST_PATH, WORD_DICT,BI_GRAM
from config import TRAIN_DATA_BI, DEV_DATA_BI, TEST_DATA_BI, TRAIN_DATA_UNI, DEV_DATA_UNI, TEST_DATA_UNI, WORD_SINGLE
from voc import Vocab, Tag
import argparse
import math
import pandas as pd


class Data_index(object):
    def __init__(self, Vocabs, tags):
        self.VOCABS = Vocabs
        self.TAGS = tags

#    def to_index_bi(self, words, tags):
#        word_idx = []
#        words.append('<EOS>')
#        words.append('<EOS>')
#        words.insert(0, '<BOS>')
#        words.insert(0, '<BOS>')
#        for i in range(2, len(words)-2):
#            for j in range(-2,3):
#                if words[i+j] in self.VOCABS.word2idx:
#                    word_idx.append(self.VOCABS.word2idx[words[i+j]])
#                else:
#                    word_idx.append(self.VOCABS.word2idx['<OOV>'])
#            for j in range(-2 , 2):
#                if words[i+j]+words[i+j+1] in self.VOCABS.word2idx:
#                    word_idx.append(self.VOCABS.word2idx[words[i+j]+words[i+j+1]])
#                else:
#                    word_idx.append(self.VOCABS.word2idx['<OOV>'])
#
#        tag_idx = [self.TAGS.tag2idx[tag] for tag in tags]
#
#        return ','.join(map(str, word_idx)), ','.join(map(str, tag_idx))
        
    def to_index_bi(self, words, tags):
        word_idx = []
        words.append('<EOS>')
        words.append('<EOS>')
        words.append('<EOS>')
        words.insert(0, '<BOS>')
        words.insert(0, '<BOS>')
        words.insert(0, '<BOS>')
        for i in range(3, len(words)-3):
            for j in range(-3,4):
                if words[i+j] in self.VOCABS.word2idx:
                    word_idx.append(self.VOCABS.word2idx[words[i+j]])
                else:
                    word_idx.append(self.VOCABS.word2idx['<OOV>'])
            for j in range(-2 , 2):
                if words[i+j]+words[i+j+1] in self.VOCABS.word2idx:
                    word_idx.append(self.VOCABS.word2idx[words[i+j]+words[i+j+1]])
                else:
                    word_idx.append(self.VOCABS.word2idx['<OOV>'])

        tag_idx = [self.TAGS.tag2idx[tag] for tag in tags]

        return ','.join(map(str, word_idx)), ','.join(map(str, tag_idx))
    
    def to_index(self, words, tags):
        word_idx = []
        for word in words:
            if word in self.VOCABS.word2idx:
                word_idx.append(self.VOCABS.word2idx[word])
            else:
                word_idx.append(self.VOCABS.word2idx['<OOV>'])

        tag_idx = [self.TAGS.tag2idx[tag] for tag in tags]

        return ','.join(map(str, word_idx)), ','.join(map(str, tag_idx))


    # This function is very special, because in the predict procedure we should concate the sub-sentence we splited
    # we should record which sentence is splited
    def process_file(self, path, output, bigram=False):
        data,label,out_of_table_num,lengths,start_with_no_chinese,end_with_no_chinese = self.process_data(path)
        index = 0

        length_symbol = []
        tag_path = path+'_tag'
        tag_output = open(tag_path , 'w')
        
        
        for words,tags in zip(data,label):
            length = len(words)
            original_length = length
            ratio = int((length-1)/MAX_LEN) + 1
            length_symbol.append(length)
            for i in range(0, ratio):
                '''
                split the long sentence whose length is larger than MAXLEN
                '''
                
                tmpwords = words[MAX_LEN*i : MAX_LEN*(i+1)]
                tmptags = tags[MAX_LEN*i : MAX_LEN*(i+1)]
                if bigram:
                    word_idx, tag_idx = self.to_index_bi(tmpwords, tmptags)
                    # 一定得保证长度，否则赋值会出错， -4 是因为要除去头尾加的EOS，BOS
                    length = len(tmpwords) - 6
                else:
                    word_idx, tag_idx = self.to_index(tmpwords, tmptags)
                    length = len(tmpwords)
                output.writerow([word_idx, tag_idx, length, math.sqrt(original_length)/10, start_with_no_chinese[index], end_with_no_chinese[index],float(out_of_table_num[index])/original_length])
            index += 1
        tag_output.write(str(length_symbol))
        tag_output.close()
        return length_symbol
                


    def process_all_data(self, bigram=False, multitask=False):
        if bigram is False:
            f_train = open(TRAIN_DATA_UNI, 'w')
            f_dev = open(DEV_DATA_UNI, 'w')
            f_test = open(TEST_DATA_UNI, 'w')
        elif multitask:
            f_train = open(TRAIN_DATA_MT, 'w')
            f_dev = open(DEV_DATA_MT, 'w')
            f_test = open(TEST_DATA_MT, 'w')
        else:
            f_train = open(TRAIN_DATA_BI, 'w')
            f_dev = open(DEV_DATA_BI, 'w')
            f_test = open(TEST_DATA_BI, 'w')
        output_train = csv.writer(f_train)
        output_train.writerow(['words', 'tags', 'length' , 'feature_length', 'start_with_no_chinese' , 'end_with_no_chinese' , 'out_of_table_num'])
        output_dev = csv.writer(f_dev)
        output_dev.writerow(['words', 'tags', 'length' , 'feature_length','start_with_no_chinese' , 'end_with_no_chinese' , 'out_of_table_num'])
        output_test = csv.writer(f_test)
        output_test.writerow(['words', 'tags', 'length' , 'feature_length','start_with_no_chinese' , 'end_with_no_chinese' , 'out_of_table_num'])
        self.process_file(TRAIN_PATH, output_train, bigram)
        self.process_file(DEV_PATH, output_dev, bigram)
        self.process_file(TEST_PATH, output_test, bigram)


        #process
        self.process_file(TRAIN_PATH, output_train, bigram)
        self.process_file(DEV_PATH, output_dev, bigram)
        self.process_file(TEST_PATH, output_test, bigram)
    


    # orignial data——> word list, tag list
    def ishan(self,text):
    # for python 3.x
    # sample: ishan('一') == True, ishan('我&&你') == False
        return all('\u4e00' <= char <= '\u9fff' for char in text)

    def process_data(self,path):
        data = []
        label = []
        out_of_table_num = []
        lengths = []
        start_with_no_chinese = []
        end_with_no_chinese = []
        f = open(path, 'r', encoding='utf-8')
        li = f.readlines()
        f.close()
    
        # process each sentence.
        for line in li:
            # a list contains words
            sentence = line.strip().replace('\n', '').replace('\r', '').replace('\u3000',' ').split(' ')
            sentence = [w for w in sentence if w != '']
            if len(sentence) == 1 and sentence[0]=='':
                continue
            if sentence == None or len(sentence)==0:
                continue
            data_sentence = []
            label_sentence = []
            for word in sentence:
                for ch in word:
                    data_sentence.append(ch)
                if(len(word)==1):
                    label_sentence.append('S')
                elif(len(word)==2):
                    label_sentence.append('B')
                    label_sentence.append('E')
                else:
                    k = len(word) - 2
                    label_sentence.append('B')
                    for i in range(k):
                        label_sentence.append('I')
                    label_sentence.append('E')
            label.append(label_sentence)
            data.append(data_sentence)
        print('processing statistics')
        
        for sentence in data:
            num = 0
            length = 0
            start_with_no = 0
            end_with_no = 0
            for i in range(len(sentence)):
                word = sentence[i]
                length += 1
                if not self.ishan(word):
                    if i == 0:
                        start_with_no = 1
                    if i == len(sentence) - 1:
                        end_with_no = 1
                    num += 1
            start_with_no_chinese.append(start_with_no)
            end_with_no_chinese.append(end_with_no)
            lengths.append(length)
            out_of_table_num.append(num)
        return data,label,out_of_table_num,lengths,start_with_no_chinese,end_with_no_chinese
        
    
    def check_data(self, path):
        with open(path, 'r',encoding='utf-8') as f:
            data = pd.read_csv(f)
            words = list(map(lambda x: list(map(int, x.split(","))), data['words'].tolist()))
            tags = list(map(lambda x: list(map(int, x.split(","))), data['tags'].tolist()))
            lengths = list(map(int, data['length'].tolist()))
            
            for i in range(data.shape[0]):
#                print(words[i])
                if BI_GRAM:
                    assert len(words[i]) == len(tags[i]) * CONCATENATE_NUM
                else:
                    assert len(words[i]) == len(tags[i])
                assert len(tags[i]) == lengths[i]
        print("the length of input is correct!")
        
if __name__ == '__main__':
    path = './data_sxu/dev'
    output = './data_sxu/dev.csv'
    f_train = open(output, 'w')
    output_train = csv.writer(f_train)
#    path = './data_sxu/dev.csv'
#    # ==================================================
#    print('Generate words and characters need to be trained')
    VOCABS = Vocab(WORD_VEC_100, WORD_SINGLE, single_task=False, bi_gram=BI_GRAM, frequency=5)
    TAGS = Tag()
    init_embedding = VOCABS.word_vectors
    da_idx = Data_index(VOCABS, TAGS)
#    da_idx = Data_index(None, None)
    da_idx.process_file(path,output_train)
#    src_data, data, label = da_idx.process_data(path)

#path = './icwb2-data/training/test.utf8'
#embedding_path = './models/sgns.renmin.bigram-char'
#embedding_path = './models/zh.vec'
#embedding_path = './models/test.rtf'

#voc = Vocab(embedding_path,WORD_SINGLE,False,BI_GRAM)
#tag = Tag()
#data_index = Data_index(voc, tag)

#src_data, data, label = data_index.process_data(path)
#word_idx, tag_idx = data_index.to_index_bi(data[0], label[0])
#data_index.process_all_data(True)
#
#for i in range(len(data)):
#    if len(data[i]) != len(label[i]):
#        print(i)
